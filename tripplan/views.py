from django.http import HttpResponse
from django.shortcuts import render
from django.template import Template, Context
import os

template = '<html>' \
           '<head>' \
           '<title>woo</title>' \
           '</head>' \
           '<body>' \
           '<h1>Hello, world</h1>' \
           '<table>' \
           '{% for k, v in items %}' \
           '<tr>' \
           '<th>' \
           '{{k}}' \
           '</th>' \
           '<td>' \
           '{{v}}' \
           '</td>' \
           '</tr>' \
           '{% endfor %}' \
           '</table>' \
           '</body></html>'


def index(request):
    t = Template(template)
    c = Context({'items': os.environ.items()})
    s = t.render(c)
    return HttpResponse(s)
