from django.apps import AppConfig


class TripplanConfig(AppConfig):
    name = 'tripplan'
