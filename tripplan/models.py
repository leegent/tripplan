from django.db import models


class Aerodrome(models.Model):
    openflights_id = models.IntegerField()
    name = models.CharField()
    country = models.CharField
    icao_code = models.CharField()
    elevation = models.FloatField()


class Trip(models.Model):
    start_date = models.DateField()
    consumption_rate = models.FloatField()
    full_fuel_volume = models.FloatField()
    intermediate_fuel_volume = models.FloatField()
    unusable_fuel_volume = models.FloatField()
    sunset = models.TimeField()


class Leg(models.Model):
    trip = models.ForeignKey('Trip', on_delete=models.CASCADE)

    start_aerodrome = models.ForeignKey('Aerodrome', on_delete=models.SET_NULL, related_name='leg_start')
    end_aerodrome = models.ForeignKey('Aerodrome', on_delete=models.SET_NULL, related_name='leg_end')

    takeoff = models.TimeField()
    fuel_at_start = models.FloatField()

